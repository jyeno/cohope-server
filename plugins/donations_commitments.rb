require 'active_record'

class DonationCommitment < ActiveRecord::Base
  has_many :donation_commitment_items, :dependent => :destroy
  belongs_to :donation
  belongs_to :committed_user, class_name: 'User'
end

class DonationCommitmentItem < ActiveRecord::Base
  belongs_to :donation_commitment
  belongs_to :donation_item
end

class CohopeServer < Sinatra::Base
  get '/donations_commitments/findByUser/:id' do
    DonationCommitment.where(committed_user_id: params[:id]).order(last_changed: :desc).to_json(
      :include => [ :donation_commitment_items => { :include => [ :donation_item ] },
                    :donation => { :include => [ :user ], :except => [ :user_id ] }
                  ],
      :except => [ :donation_id, :committed_user_id ]
    )
  end

  post '/donations_commitments' do
    begin
      payload = JSON.parse(request.body.read)
      donation = Donation.find(payload['donation_id'])
      if donation.user.id == payload['committed_user_id']
        return '{ "message": "Não pode doar/receber de si próprio." }'
      end
      if payload['anonymous'] == 'true' and donation.kind == 'O'
        return '{ "message": "Não é possível fazer recepção anônima." }'
      end
      user = User.find(payload['committed_user_id'])
      donationCommitment = DonationCommitment.new(donation_id: payload['donation_id'],
                                                  committed_user_id: payload['committed_user_id'],
                                                  anonymous: payload['anonymous'],
                                                  last_changed: Time.now)
      payload['donation_commitment_items'].each do |item|
        donationItem = DonationItem.find(item['donation_item_id'])
        receivedAmount = item['amount']
        if receivedAmount <= 0 || receivedAmount > donationItem.amount
          donationCommitment.destroy
          return '{ "message": "Quantidade inválida, não pode ser zero nem maior que a quantidade solicitada." }'
        end
        donationCommitment.donation_commitment_items << DonationCommitmentItem.create(donation_item: donationItem,
                                                                                      amount: receivedAmount)
      end
      donationCommitment.save
      donationCommitment.to_json(:include => [ :donation, :committed_user, :donation_commitment_items ], :except => [ :donation_id, :committed_user_id ])
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Doação, item ou usuário não encontrado(a)!" }'
    end
  end

  put '/donations_commitments/:id' do
    begin
      payload = JSON.parse(request.body.read)
      donationCommitment = DonationCommitment.find(params[:id])
      if Donation.find(payload['donation_id']).user.id == payload['committed_user_id']
        return '{ "message": "Não pode doar/receber de si próprio." }'
      end
      user = User.find(payload['committed_user_id'])
      donationCommitment.donation_id = payload['donation_id']
      donationCommitment.committed_user_id = payload['committed_user_id']
      donationCommitment.last_changed = Time.now
      donationCommitment.anonymous = payload['anonymous']
      donationCommitment.donation_commitment_items.destroy_all
      payload['donation_commitment_items'].each do |item|
        donationItem = DonationItem.find(item['donation_item_id'])
        receivedAmount = item['amount']
        if receivedAmount <= 0 || receivedAmount > donationItem.amount
          donationCommitment.destroy
          return '{ "message": "Quantidade inválida, não pode ser zero nem maior que a quantidade solicitada." }'
        end
        donationCommitment.donation_commitment_items << DonationCommitmentItem.create(donation_item: donationItem,
                                                                                      amount: receivedAmount)
      end
      donationCommitment.save
      donationCommitment.to_json(:include => [ :donation, :committed_user, :donation_commitment_items ], :except => [ :donation_id, :committed_user_id ])
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Compromisso de doação, doação, item ou usuário não encontrado." }'
    end
  end

  delete '/donations_commitments/:id' do
    begin
      DonationCommitment.find(params[:id]).destroy
      '{ "message": "Compromisso de doação cancelado." }'
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Compromisso de doação não encontrado." }'
    end
  end
end
