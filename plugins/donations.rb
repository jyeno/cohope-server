require 'active_record'
require 'firebase_cloud_messenger'

FirebaseCloudMessenger.credentials_path = ENV["FIREBASE_SVCACCOUNT_FILE"]

class Donation < ActiveRecord::Base
  has_many :donation_commitments, :dependent => :destroy
  has_many :donation_items, :dependent => :destroy
  belongs_to :user
end

class DonationItem < ActiveRecord::Base
  belongs_to :donation
end

class CohopeServer < Sinatra::Base
  get '/donations' do
    Donation.all.order(id: :desc).to_json(:include => [ :user, :donation_items, :donation_commitments => { :include => [ :donation_commitment_items ] } ],
                                          :except => [ :user_id ])
  end

  get '/donations/findByKind' do
    if params[:kind] == nil
      '{ "message": "Você precisa informar o tipo da doação [e o id do usuário]!" }'
    else
      if params[:user_id] == nil or params[:user_id] == "-1"
        Donation.where(kind: params[:kind]).order(id: :desc).to_json(
          :include => [ :user, :donation_items, :donation_commitments => { :include => [ :donation_commitment_items, :committed_user ] } ], :except => [ :user_id ])
      else
        Donation.where(kind: params[:kind]).where(user_id: params[:user_id]).order(id: :desc).to_json(
          :include => [ :user, :donation_items, :donation_commitments => { :include => [ :donation_commitment_items, :committed_user ] } ], :except => [ :user_id ])
      end
    end
  end

  post '/donations' do
    begin
      payload = JSON.parse(request.body.read)
      user = User.find(payload['user_id'])
      donation = Donation.create(name: payload['name'], kind: payload['kind'])
      donation.user = user
      payload['donation_items'].each do |item|
        donation.donation_items << DonationItem.create(description: item['description'],
                                                      photo_url: item['photo_url'],
                                                      unity: item['unity'],
                                                      amount: item['amount'])
      end
      donation.save
      firebaseMessage = {
        notification: {
          title: 'Nova ' + ((payload['kind'] == "D") ? 'demanda':'oferta') + ' de doação criada!',
          body: 'Clique aqui para ver a ' + ((payload['kind'] == "D") ? 'demanda':'oferta') + ' de doação criada por \'' + user.display_name + '\'!'
        },
        data: {
          "donation_kind": payload['kind']
        },
        condition: "!('dummy' in topics)",
      }
      FirebaseCloudMessenger.send(message: firebaseMessage)
      donation.to_json(:include => [ :donation_items ], :except => [ :user_id ])
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Usuário não encontrado!" }'
    end
  end

  put '/donations/:id' do
    begin
      payload = JSON.parse(request.body.read)
      donation = Donation.find(params[:id])
      user = User.find(payload['user_id'])
      donation.name = payload['name']
      donation.kind = payload['kind']
      donation.user = user
      donation.donation_items.destroy_all
      payload['donation_items'].each do |item|
        donation.donation_items << DonationItem.create(description: item['description'],
                                                      photo_url: item['photo_url'],
                                                      unity: item['unity'],
                                                      amount: item['amount'])
      end
      donation.save
      donation.to_json(:include => [ :donation_items ], :except => [ :user_id ])
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Doação ou usuário não encontrado!" }'
    end
  end

  delete '/donations/:id' do
    begin
      Donation.find(params[:id]).destroy
      '{ "message": "Doação removida com sucesso!" }'
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Doação não encontrada!" }'
    end
  end
end
