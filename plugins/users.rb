require 'active_record'

class User < ActiveRecord::Base
end

class CohopeServer < Sinatra::Base
  get '/users' do
    User.all.to_json
  end

  get '/users/:email' do
    user = User.find_by_email(params[:email])
    !user ? '{ "message": "Usuário não encontrado!" }':user.to_json
  end

  post '/users' do
    payload = JSON.parse(request.body.read)
    begin
      User.create(email: payload['email'],
                  display_name: payload['display_name'],
                  photo_url: payload['photo_url'],
                  website: payload['website'],
                  bio: payload['bio'],
                  amount_finished_offers: 0,
                  amount_finished_demands: 0
                ).to_json
    rescue ActiveRecord::RecordNotUnique
      User.find_by_email(payload['email']).to_json
    end
  end

  put '/users/:id' do
    payload = JSON.parse(request.body.read)
    begin
      user = User.find(params[:id])
      user.email = payload['email']
      user.display_name = payload['display_name']
      user.photo_url = payload['photo_url']
      user.website = payload['website']
      user.bio = payload['bio']
      user.amount_finished_offers = payload['amount_finished_offers']
      user.amount_finished_demands = payload['amount_finished_demands']
      user.save
      user.to_json
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Usuário não encontrado!" }'
    end
  end

  delete '/users/:id' do
    begin
      user = User.find(params[:id])
      user.destroy
      '{ "message": "Usuário removido com sucesso!" }'
    rescue ActiveRecord::RecordNotFound
      status 404
      '{ "message": "Usuário não encontrado!" }'
    end
  end
end
