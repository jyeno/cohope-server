class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email, null: false, index: { unique: true }
      t.string :display_name, null: false
      t.string :photo_url
      t.string :website
      t.text :bio
      t.integer :amount_finished_offers
      t.integer :amount_finished_demands
    end
  end
end
