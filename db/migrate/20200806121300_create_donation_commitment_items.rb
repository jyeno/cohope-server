class CreateDonationCommitmentItems < ActiveRecord::Migration[5.2]
  def change
    create_table :donation_commitment_items do |t|
      t.references :donation_commitment, index: true, foreign_key: true
      t.references :donation_item, index: true, foreign_key: true
      t.integer :amount, null: false
    end
  end
end
