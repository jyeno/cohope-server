class ChangeDonationCommitments < ActiveRecord::Migration[6.0]
  def change
    change_table :donation_commitments do |t|
      t.boolean :anonymous, default: false
    end
  end
end
