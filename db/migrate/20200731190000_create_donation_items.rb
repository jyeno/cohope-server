class CreateDonationItems < ActiveRecord::Migration[5.2]
  def change
    create_table :donation_items do |t|
      t.string :description, null: false
      t.string :photo_url
      t.string :unity, null: false
      t.integer :amount, null: false
      t.references :donation, index: true, foreign_key: true
    end
  end
end
