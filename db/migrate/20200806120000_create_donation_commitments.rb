class CreateDonationCommitments < ActiveRecord::Migration[5.2]
  def change
    create_table :donation_commitments do |t|
      t.references :donation, index: true, foreign_key: true
      t.references :committed_user, index: true, foreign_key: { to_table: 'users' }
      t.datetime :last_changed
    end
  end
end
