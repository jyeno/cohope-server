class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :name, null: false
      t.string :kind, limit: 1, index: true, null: false
      t.references :user, index: true, foreign_key: true
    end
  end
end
