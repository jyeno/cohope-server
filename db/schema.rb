# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_19_170000) do

  create_table "donation_commitment_items", force: :cascade do |t|
    t.integer "donation_commitment_id"
    t.integer "donation_item_id"
    t.integer "amount", null: false
    t.index ["donation_commitment_id"], name: "index_donation_commitment_items_on_donation_commitment_id"
    t.index ["donation_item_id"], name: "index_donation_commitment_items_on_donation_item_id"
  end

  create_table "donation_commitments", force: :cascade do |t|
    t.integer "donation_id"
    t.integer "committed_user_id"
    t.datetime "last_changed"
    t.boolean "anonymous", default: false
    t.index ["committed_user_id"], name: "index_donation_commitments_on_committed_user_id"
    t.index ["donation_id"], name: "index_donation_commitments_on_donation_id"
  end

  create_table "donation_items", force: :cascade do |t|
    t.string "description", null: false
    t.string "photo_url"
    t.string "unity", null: false
    t.integer "amount", null: false
    t.integer "donation_id"
    t.index ["donation_id"], name: "index_donation_items_on_donation_id"
  end

  create_table "donations", force: :cascade do |t|
    t.string "name", null: false
    t.string "kind", limit: 1, null: false
    t.integer "user_id"
    t.index ["kind"], name: "index_donations_on_kind"
    t.index ["user_id"], name: "index_donations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "display_name", null: false
    t.string "photo_url"
    t.string "website"
    t.text "bio"
    t.integer "amount_finished_offers"
    t.integer "amount_finished_demands"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "donation_commitment_items", "donation_commitments"
  add_foreign_key "donation_commitment_items", "donation_items"
  add_foreign_key "donation_commitments", "donations"
  add_foreign_key "donation_commitments", "users", column: "committed_user_id"
  add_foreign_key "donation_items", "donations"
  add_foreign_key "donations", "users"
end
