FROM ruby:alpine

LABEL maintainer="jeno.andrade@gmail.com"

RUN apk update && \
    apk upgrade && \
    apk add build-base mariadb-dev sqlite-dev && \
    rm -rf /var/cache/apk/*

# Set environment variables.
ENV HOME /var/app
ENV FIREBASE_SVCACCOUNT_FILE /var/app/cohope-client-firebase-adminsdk-zkgud-690c78ad8b.json

# Define working directory.
COPY . /var/app
RUN cd /var/app && bundle install

WORKDIR /var/app

EXPOSE 4000

CMD ["/bin/sh", "/var/app/bin/start.sh"]
