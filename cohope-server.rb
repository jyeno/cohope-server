require 'sinatra/base'
require 'sinatra/activerecord'
require 'sinatra/cors'

@environment = ENV['RACK_ENV'] || 'development'
@dbconfig = YAML.load(ERB.new(File.read('db/config.yml')).result)
ActiveRecord::Base.establish_connection @dbconfig[@environment]

class CohopeServer < Sinatra::Base
  register Sinatra::Cors

  set :allow_origin, "*"
  set :allow_methods, "GET,POST,PUT,DELETE"
  set :allow_headers, "content-type,if-modified-since"
  set :expose_headers, "location,link"
end

current_dir = Dir.pwd
Dir["#{current_dir}/plugins/*.rb"].each { |file| require file }
